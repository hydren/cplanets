/*
 * SDL_utils.cpp
 *
 *  Created on: 13 de abr de 2016
 *      Author: carlosfaruolo
 */

#include "SDL_util.hpp"

#include "hotfixes.h" //macros to enable/disable hotfixes for SDL-related issues

#include <cstdlib>

#include "futil/language.hpp"
#include "futil/random.h"

// case for old SDL versions
#ifndef SDL_putenv && defined(POSIX_COMPLIANT_OS)
	#define SDL_putenv putenv
#endif

int SDL_util::colorToInt(const SDL_Surface* surf, const SDL_Color& color, bool forceRGBA)
{
	#ifdef HOTFIX_FOR_SDL_MAP_RGB_1
	if(forceRGBA || surf == null)
		return (((int) color.r) << 24) + (((int) color.g) << 16) + (((int) color.b) << 8) + 0x000000FF;
	#endif

	return SDL_MapRGB(surf->format, color.r, color.g, color.b);
}

SDL_Color SDL_util::getRandomColor()
{
	SDL_Color somecolor;
	somecolor.r = futil::random_between(0, 255);
	somecolor.g = futil::random_between(0, 255);
	somecolor.b = futil::random_between(0, 255);
	return somecolor;
}

SDL_Color* SDL_util::getRandomColor(SDL_Color* colorPtr)
{
	if(colorPtr == null)
		colorPtr = new SDL_Color();

	colorPtr->r = futil::random_between(0, 255);
	colorPtr->g = futil::random_between(0, 255);
	colorPtr->b = futil::random_between(0, 255);

	return colorPtr;
}

SDL_Surface* SDL_util::loadBitmap(const char* path, const SDL_Color* transparentColor)
{
	SDL_Surface* bmp = SDL_LoadBMP(path);
	if(bmp != null && transparentColor != null)
		SDL_SetColorKey(bmp, SDL_SRCCOLORKEY, SDL_MapRGB(bmp->format, transparentColor->r, transparentColor->g, transparentColor->b));

	return bmp;
}

#ifdef _WIN32
	#include <windows.h>
#endif

#if !defined(_WIN32) && (defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__)))
	#include <unistd.h>
	#if defined(_POSIX_VERSION)
		#define POSIX_COMPLIANT_OS
	#endif
#endif

#ifdef SDL_putenv
	// needed for SDL_putenv
	static char cStrEnvVarSdlVideoCentered[32], cStrEnvVarSdlVideoWindowPos[32];
#endif

void SDL_util::preloadCentered()
{
	#ifdef SDL_putenv
		// sets SDL_VIDEO_CENTERED as "center"
		sprintf(cStrEnvVarSdlVideoCentered, "SDL_VIDEO_CENTERED=center");
		SDL_putenv(cStrEnvVarSdlVideoCentered);

		// resets SDL_VIDEO_WINDOW_POS
		strcpy(cStrEnvVarSdlVideoWindowPos, "");

	#elif defined(_WIN32)
		SetEnvironmentVariable("SDL_VIDEO_CENTERED", "1");
		SetEnvironmentVariable("SDL_VIDEO_WINDOW_POS", null);

	#endif
}

void SDL_util::preloadPosition(int x, int y)
{
	#ifdef SDL_putenv
		// resets SDL_VIDEO_CENTERED
		strcpy(cStrEnvVarSdlVideoCentered, "");

		// sets SDL_VIDEO_WINDOW_POS coordinates
		sprintf(cStrEnvVarSdlVideoWindowPos, "SDL_VIDEO_WINDOW_POS=%d,%d", x, y);
		SDL_putenv(cStrEnvVarSdlVideoWindowPos);

	#elif defined(_WIN32)
		static char tmp[16];
		sprintf(tmp, "%d,%d", x, y);
		SetEnvironmentVariable("SDL_VIDEO_CENTERED", null);
		SetEnvironmentVariable("SDL_VIDEO_WINDOW_POS", tmp);

	#endif
}
