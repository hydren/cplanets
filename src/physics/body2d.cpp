/*
 * body2d.cpp
 *
 *  Created on: 20/04/2015
 *      Author: felipe
 */

#include "body2d.hpp"

#include "futil/language.hpp"
#include "futil/string_extra_operators.hpp"

#include <cmath>

using std::string;

Body2D::Body2D()
: id(), mass(), diameter(), position(), velocity(), acceleration(), userObject(null)
{}

Body2D::Body2D(double mass, double diameter, Vector2D position, Vector2D velocity, Vector2D acceleration)
: id(), mass(mass), diameter(diameter), position(position), velocity(velocity), acceleration(acceleration), userObject(null)
{}

Body2D::Body2D(string id, double mass, double diameter, Vector2D position, Vector2D velocity, Vector2D acceleration)
: id(id), mass(mass), diameter(diameter), position(position), velocity(velocity), acceleration(acceleration), userObject(null)
{}

string Body2D::toString()
{
	return id + " ("+mass+"Kg, "+diameter+"Km)";
}

Vector2D Body2D::momentum() const
{
	return velocity.times(mass);
}

double Body2D::kineticEnergy() const
{
	return pow(velocity.magnitude(), 2) * mass * 0.5;
}
